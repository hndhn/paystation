package paystation.domain;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

public class TestChangeStation {

	PayStation ps;

	@Test
	public void shouldKeepIncreaseTimeWhenChangingStationIntegration() 
		    throws IllegalCoinException {
		    ps = new PayStationImpl( new BetaTownFactory() );
			ps.addPayment(10);
		    ps.addPayment(10);
		    ps.addPayment(10);
		    ps.addPayment(10);
		    ps.addPayment(10);
		    ps.addPayment(25);
		    ps.addPayment(25);
		    ps.addPayment(25);
		    ps.addPayment(25);
		    assertEquals( "Linear rate should have 60 min",
	                  60 , ps.readDisplay() );
		    ps.changeStation(new AlphaTownFactory());

		    ps.addPayment(25);
		    ps.addPayment(25);
//		    assertEquals( "Progressive rate should have 120 min",
//	                  120 , ps.readDisplay() );
		    int minutes = 200/5*2;
		    Calendar now = GregorianCalendar.getInstance();
		    now.add(Calendar.MINUTE,minutes);
		    int result =
		      now.get(Calendar.HOUR_OF_DAY) * 100 
		      + 
		      now.get(Calendar.MINUTE);
		    assertEquals("80 min from now should be"+result, 
	                 result, ps.readDisplay());
		  } 
}


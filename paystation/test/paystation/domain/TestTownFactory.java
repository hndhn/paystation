package paystation.domain;
/** Factory for making the pay station configuration
    for unit testing pay station behavior.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
class TestTownFactory implements PayStationFactory {
  public RateStrategy createRateStrategy() {
    return new One2OneRateStrategy();
  }
  public Receipt createReceipt( int parkingTime ) {
    return new StandardReceipt(parkingTime);
  }
  public DisplayStrategy createDisplayStrategy() {
    return new ValueDisplayStrategy();
  }
}
